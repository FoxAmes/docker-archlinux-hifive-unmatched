FROM archlinux

# Install Devtools
RUN pacman -Sy --noconfirm asp base-devel devtools expac

COPY riscv64-unmatched-linux-gnu-gcc-12.2.0-1-x86_64.pkg.tar.zst /tmp/
RUN pacman -U --noconfirm /tmp/riscv64-unmatched-linux-gnu-gcc-12.2.0-1-x86_64.pkg.tar.zst
RUN cp -rn /usr/include/* /usr/riscv64-unmatched-linux-gnu/sysroot/usr/include/

RUN useradd -mG wheel -d /build build
RUN echo "%wheel ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers
RUN mkdir /pkgs && chown build /pkgs
ADD makepkg.conf /etc/makepkg.d/riscv64-unmatched.conf
ADD patch-arch.sh /usr/bin/
ADD patch-cross.sh /usr/bin/
ADD build-helper.sh /usr/bin/
ADD riscv64-unmatched-linux-gnu.txt /usr/share/meson/

#ADD install-builddeps.sh /usr/bin
#RUN install-builddeps.sh

USER build
WORKDIR /build

# PGP keys
# archlinux-keyring, bash, bzip2, file, findutils, gawk, grep, procps-ng, tar, pciutils, shadow, xz, iputils, iproute2, linux, libmnl, libcap, less, hwdata, zlib, attr, ncurses
RUN gpg --recv-keys 6D42BDD116E0068F BB5869F064EA74AB FC57E3CCACD99A78 71112AB16CB33B3A 46502EF796917195 DF597815937EC0D2 7FD9FCCB000BEEEE 022166C0FF3C84E3 3602B07F55D0C732 C28E7847ED70F82D 3570DA17270ACE24 38EE757D69184620 C0DEC2EE72F33A5F 80A77F6095CDE47E 38DBBDC86092693E D55D978A8A1420E4 29EE848AE2CCF3F4 F153A7C833235259 19C2F062574F5403 783FCD8E58BCAFBA D5BF9FEB0313653A CC2AF4472167BE03

# Build base packages
#RUN \
#for pkg in $(expac -S '%E' base) ; do \
#  echo "Building $pkg" && \
#  build-helper.sh $pkg \
#; done

