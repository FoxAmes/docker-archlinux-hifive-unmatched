#!/bin/sh

for pkg in $(expac -S '%E' base) ; do
  mdeps=$(asp show $pkg 2>/dev/null | grep -zoP 'makedepends=\([^\)]+' | sed -z 's/'"'"'\([^ ]\+\)'"'"'/\1/g; s/\s\+/ /g; s/^makedepends[^(]\+(//')
  if [ ! -z "$mdeps" ] ; then
    pacman -S --noconfirm --needed $mdeps
  fi
done
