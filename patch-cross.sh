#!/bin/sh

printhelp() {
  echo "Usage: $0 [host-tuple] [PKGBUILD]"
  echo "Patches a PKGBUILD with common cross-compile fixes."
}

# Check we have arguments
if [ $# -lt 2 ]; then
  printhelp
  exit 1
fi

# Check we were given a file
if ! [ -f $2 ]; then
  echo "$2 is not a file."
  printhelp
  exit 2
fi

# Patch configure options
sed -i 's/\.\/configure/.\/configure --host='"'$1'"'/g' $2

# Patch hardcoded GCC calls
sed -i 's/\([^\w-]\)gcc /\1'"$1"'-gcc /g' $2

# Patch meson builds
sed -i 's/arch-meson /arch-meson --cross-file \/usr\/share\/meson\/riscv64-unmatched-linux-gnu.txt /' $2
sed -i 's/meson setup /meson setup --cross-file \/usr\/share\/meson\/riscv64-unmatched-linux-gnu.txt /' $2
