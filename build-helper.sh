#!/bin/sh

printhelp() {
  echo "Usage: $0 [package]"
}

if [ $# -lt 1 ]; then
  printhelp
  exit 1
fi

# Check for existing dir
if [ -d $1 ]; then exit 0; fi

# Checkout package
co=$(asp checkout $1)
if [ $? -ne 0 ]; then
  echo "Unable to checkout $1, failing"
  exit 2
fi

# Check if package redirects
cdir=$1
if echo $co | grep "$1 is part of package"; then
  cdir=$(echo $co | grep 'is part of package' | sed 's/.*'"$1"' is part of package \(.*\)/\1/')
  # Check for split packages we've already built
  if [ -d $cdir ]; then exit 0; fi
fi

set -e

# Get build dependencies
#mdeps=$(asp show $1 | grep -zoP 'makedepends=\([^\)]+' | sed -z "s/'\([^ ]\+\)'/\1/g; s/\s\+/ /g; s/^makedepends[^(]\+(//")
deps=$(expac -S '%E' $1)

# Build deps
echo "Building dependencies for $1"
for dep in $deps; do
  $0 $dep
done

# Build
cd $cdir/trunk/
patch-arch.sh riscv64 PKGBUILD
patch-cross.sh riscv64-unmatched-linux-gnu PKGBUILD
makepkg -fs --noconfirm --config /etc/makepkg.d/riscv64-unmatched.conf

# Copy build libraries/includes to toolchain sysroot
if [ -d "pkg/$cdir/lib" ]; then cp -r "pkg/$cdir/lib/"* /usr/riscv64-unmatched-linux-gnu/sysroot/lib; fi
if [ -d "pkg/$cdir/usr/lib" ]; then cp -r "pkg/$cdir/usr/lib/"* /usr/riscv64-unmatched-linux-gnu/sysroot/usr/lib; fi
if [ -d "pkg/$cdir/include" ]; then cp -r "pkg/$cdir/include/"* /usr/riscv64-unmatched-linux-gnu/sysroot/include; fi
if [ -d "pkg/$cdir/usr/include" ]; then cp -r "pkg/$cdir/usr/include/"* /usr/riscv64-unmatched-linux-gnu/sysroot/usr/include; fi
