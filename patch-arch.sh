#!/bin/sh

printhelp() {
  echo "Usage: $0 [arch] [PKGBUILD path]"
  echo "Example: $0 mips ./PKGBUILD"
  echo "Patches a PKGBUILD to support a given architecture."
}

if [ $# -lt 2 ]; then
  printhelp
  exit 1
fi

if ! [ -f $2 ]; then
  echo "$2 is not a file"
  printhelp
  exit 2
fi

if grep "^arch=" $2 | grep "[^\w]any[^\w]"; then
  echo "$2 targets the any architecture, no patch necessary."
  exit 0
fi

if grep "^arch=" $2 | grep "$1"; then
  echo "$2 already targets $1, no patch necessary."
  exit 0
fi

sed -E 's/^arch= *\( *((:?'"'"'?\w*'"'"'?)+) *\)/arch=(\1 '"'$1'"')/' -i $2
